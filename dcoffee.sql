--
-- File generated with SQLiteStudio v3.4.4 on จ. ก.ย. 11 16:16:26 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
DROP TABLE IF EXISTS category;

CREATE TABLE IF NOT EXISTS category (
    category_id   INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    category_name TEXT (50) NOT NULL
);

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         1,
                         'Coffee'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         2,
                         'Bakery'
                     );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE IF NOT EXISTS product (
    product_id          INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    product_name        TEXT (50) UNIQUE,
    product_price       DOUBLE    NOT NULL,
    PRODUCT_SIZE        TEXT (5)  DEFAULT SML
                                  NOT NULL,
    product_sweet_level TEXT (5)  DEFAULT (123) 
                                  NOT NULL,
    product_type        TEXT (5)  DEFAULT HCF
                                  NOT NULL,
    category_id         INTEGER   DEFAULT (1) 
                                  NOT NULL
                                  REFERENCES category (cate_id) ON DELETE RESTRICT
                                                                ON UPDATE CASCADE
);

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        PRODUCT_SIZE,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        1,
                        'Espresso',
                        30.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        PRODUCT_SIZE,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        2,
                        'Americano',
                        40.0,
                        'SML',
                        '012',
                        'HC',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        PRODUCT_SIZE,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        3,
                        'Damn Cake',
                        50.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        PRODUCT_SIZE,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        4,
                        'Butter Cake',
                        60.0,
                        '-',
                        '-',
                        '-',
                        2
                    );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
