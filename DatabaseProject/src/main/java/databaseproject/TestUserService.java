/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package databaseproject;

import databaseproject.model.User;
import databaseproject.sevice.UserService;

/**
 *
 * @author Nobpharat
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("User2","password");
        
        if( user != null){
            System.out.println("Welcome User : "+ user.getName());
            
        }else{
            System.out.println("Opp! Someting wrong");
        }
    }
}
