/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package databaseproject;

import databaseproject.dao.UserDao;
import databaseproject.helper.DataBaseHelper;
import databaseproject.model.User;

/**
 *
 * @author Nobpharat
 */
public class Test {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for(User u: userDao.getAll()){
            System.out.println(u);
        }
        
//        User user1 = userDao.get(2);
//        System.out.println(user1);
        
//        User newUser = new User("User6","password",2,"F");
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);
//        insertedUser.setGender("M");
//        user1.setGender("F");
//        userDao.update(user1);
//        User updateUser = userDao.get(user1.getId());
//        System.out.println(updateUser);
//        userDao.delete(user1);
//        for(User u: userDao.getAll()){
//            System.out.println(u);
//        }
        
        
        
        for(User u: userDao.getAllOderBy("user_name", "asc")){
            System.out.println(u);
        }
        for(User u: userDao.getAll("user_name like 'u%' ", " user_name asc, user_gender desc")){
            System.out.println(u);
        }
        DataBaseHelper.close();
    }
}
